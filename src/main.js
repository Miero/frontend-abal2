import Vue from "vue";
import JsonExcel from "vue-json-excel";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.component("downloadExcel", JsonExcel);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
